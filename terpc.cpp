#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <terpwin.h>
#include <instruct.h>

using namespace std;

#define rd()	window->refreshData(ip-1, dp, next_card-1)

// --- COMPUTER MEMORY STRUCTURES

int  data[1000]; // actual data memory

INSTRUCTIONS instrmem[1000]; // INSTRUCTION MEMORY SPACE

// --- CARD READER SUPPORT
int card[1000];
int next_card = 0;

// --- INTERPRETER SUPPORT
int ip = 0;
int dp = 0; //data pointer
string status = "";
//======================================

//===== PROTOTYPES =====================
int main(int, char**);
void loader(int datamem[],INSTRUCTIONS instrmem[],int cardreader[], string);
void parse_instruction(char instr[], char &sign, char &oper, int &op1, int &op2, int &op3);
void p0(int op1, int op2, int op3);
void p1(int op1, int op2, int op3);
void p2(int op1, int op2, int op3);
void p3(int op1, int op2, int op3);
void p4(int op1, int op2, int op3);
void p5(int op1, int op2, int op3);
void p6(int op1, int op2, int op3);
void p7(int op1, int op2, int op3);
void p8(int op1, int op2, int op3);
void p9(int op1, int op2, int op3);
void n0(int op1, int op2, int op3);
void n1(int op1, int op2, int op3);
void n2(int op1, int op2, int op3);
void n3(int op1, int op2, int op3);
void n4(int op1, int op2, int op3);
void n5(int op1, int op2, int op3);
void n6(int op1, int op2, int op3);
void n7(int op1, int op2, int op3);
void n8(int op1, int op2, int op3);
void n9(int op1, int op2, int op3);


// Initialize ncurses object
TerpWindow * window = new TerpWindow(data, card, instrmem);

// =========  main()  ==============
int main(int argc, char **argv)
{
	INSTRUCTIONS current_instruction;
   	int op1;
	int op2;
   	int op3;
   	string fname;
   	int skip = 0;

   	if (argc < 2)
   	{
	   	cout << "Error: No input file provided." << endl;
	   	cout << "Please enter input filename:";
	   	cin >> fname;
   	}
   	else { fname = argv[1];}

   	// LOAD CARDS
   	loader(data,instrmem,card, fname);

   	// Initialize ncurses handler
   	window->initialize();
   	rd();

   	while (1)    // Execution cycle
   	{

	//Fetch Instruction
	current_instruction = instrmem[ip];
	op1 = current_instruction.op1;
	op2 = current_instruction.op2;
	op3 = current_instruction.op3;

	// Increment IP
	ip++;

	  	switch (current_instruction.sign)
	  	{
	    	case '+' : switch (current_instruction.oper)
		            {
					   case '0' : p0(op1,op2,op3); break;
					   case '1' : p1(op1,op2,op3); break;
					   case '2' : p2(op1,op2,op3); break;
					   case '3' : p3(op1,op2,op3); break;
					   case '4' : p4(op1,op2,op3); skip=1; break;
					   case '5' : p5(op1,op2,op3); skip=1; break;
					   case '6' : p6(op1,op2,op3); break;
					   case '7' : p7(op1,op2,op3); skip=1; break;
					   case '8' : p8(op1,op2,op3); break;
					   case '9' : p9(op1,op2,op3); break;
					}
		            break;
		case '-' :  switch (current_instruction.oper)
		            {
					   case '0' : n0(op1,op2,op3); break;
					   case '1' : n1(op1,op2,op3); break;
					   case '2' : n2(op1,op2,op3); break;
					   case '3' : n3(op1,op2,op3); break;
					   case '4' : n4(op1,op2,op3); skip=1; break;
					   case '5' : n5(op1,op2,op3); skip=1; break;
					   case '6' : n6(op1,op2,op3); break;
					   case '7' : n7(op1,op2,op3); break;
					   case '8' : n8(op1,op2,op3); break;
					   case '9' : n9(op1,op2,op3); break;
					}
		            break;
	   }
	   if(!skip)
	   {
	   	rd();
	   }
           skip = 0;
	};// while

   	delete[] window;
   	return 0;
}

//================ Parse Instruction  ================================
void parse_instruction(char *instr, INSTRUCTIONS *instr_set, int instr_number)
{
	char temp[4];

	// parse sign
	instr_set[instr_number].sign = instr[0];

	//parse operator
	instr_set[instr_number].oper = instr[1];

	// parse operands
	temp[3] = '\0';
   	for (int i = 0; i<3; i++)
        	temp[i] = instr[i+2];
    	instr_set[instr_number].op1 = atoi(temp);
   	for (int i = 0; i<3; i++)
        	temp[i] = instr[i+5];
   	instr_set[instr_number].op2 = atoi(temp);
	for (int i = 0; i<3; i++)
          	temp[i] = instr[i+8];
   	instr_set[instr_number].op3 = atoi(temp);
}
// ===========  INTER OPERATIONS ============================
// =========

void p0(int op1, int op2, int op3)
{
	window->status = string("Moving value ") + to_string(data[op1]) +
		         string(" to location ") + to_string(op3);

  	data[op3] = data[op1];
	dp = op3;
}
// =========
void p1(int op1, int op2, int op3)
{
    	window->status = string("Adding ") + to_string(data[op1]) +
		         string(" and ") + to_string(data[op2]) +
			 string(" and placing result in location ") +
			 to_string(op3);

    	data[op3] = data[op1] + data[op2];
    	dp = op3;
}
// =========
void p2(int op1, int op2, int op3)
{
    	window->status = string("Multiplying ") + to_string(data[op1]) +
		         string(" and ") + to_string(data[op2]) +
			 string(" and placing result in location") +
			 to_string(op3);

    	data[op3] = data[op1] * data[op2];
    	dp = op3;
}
// =========
void p3(int op1, int op2, int op3)
{
	window->status = string("Squaring ") + to_string(data[op1]) +
			 string(" and moving result to location ") + to_string(op3);

    	data[op3] = data[op1] * data[op1];
    	dp = op3;
}
// =========
void p4(int op1, int op2, int op3)
{
	window->status = string("Checking if ") + to_string(data[op1]) +
		         string(" equals ") + to_string(data[op2]) + string("... ");

	window->refreshData(ip-1, dp, next_card-1);

	if ( data[op1] == data[op2])
	{
		window->status = window->status + string("True! Jumping to L") + to_string(op3);
	   	ip = op3;
    	}
    	else
    	{
	    	window->status = window->status + string("False, moving to next instruction.");
    	}
}
// =========
void p5(int op1, int op2, int op3)
{
	window->status = string("Checking if ") + to_string(data[op1]) +
		         string(" is greater than or equal to ") +
			 to_string(data[op2]) + string("... ");

	window->refreshData(ip-1, dp, next_card-1);

   	if ( data[op1] >= data[op2])
	{
	   	window->status = window->status + string("True! Jumping to L") + to_string(op3);
	   	ip = op3;
    	}
    	else
    	{
	    	window->status = window->status + string("False, moving to next instruction.");
    	}
}
// =========
void p6(int op1, int op2, int op3)
{
    	window->status = string("Moving value ") + to_string(data[op1+data[op2]]) +
		         string(" to location ") + to_string(op3);

    	data[op3] = data[op1+data[op2]];
    	dp = op3;
}
// =========
void p7(int op1, int op2, int op3)
{
	window->status = string("Incrementing value located in ") + to_string(op1) +
		         string(" and testing if ") + to_string(data[op1]) +
			 string(" is less than ") + to_string(data[op2]) + string("... ");

	data[op1] = data[op1] + 1;
	dp = op1;

	window->refreshData(ip-1, dp, next_card-1);

	if (data[op1] < data[op2])
	{
		window->status = window->status + string("True! Jumping to L") + to_string(op3);
		ip = op3;
		window->refreshData(ip, dp, next_card-1);
	}
	else
	{
		window->status = window->status + string("False, moving to next instruction.");
	}
}
// =========
void p8(int op1, int op2, int op3)
{
    	window->status = string("Reading next input card and moving value ") +
		         to_string(card[next_card]) + string(" to location ") + to_string(op3);

    	data[op3] = card[next_card++];
    	dp = op3;
}
// =========
void p9(int op1, int op2, int op3)
{
   	window->status = string("Stopping!");
   	window->refreshData(ip-1, dp, next_card-1);
   	exit(1);
}
// =========
void n0(int op1, int op2, int op3)
{
	window->status = string("Unused operation (-0)");
}
// =========
void n1(int op1, int op2, int op3)
{
	window->status = string("Subtracting values ") + to_string(data[op1]) +
		         string(" and ") + to_string(data[op2]) +
			 string(" and placing result in ") + to_string(op3);

    	data[op3] = data[op1] - data[op2];
    	dp = op3;
}
// =========
void n2(int op1, int op2, int op3)
{
	window->status = string("Dividing ") + to_string(data[op2]) +
		         string(" from ") + to_string(data[op1]) +
			 string(" and placing result in ") + to_string(op3);

    	data[op3] = data[op1] / data [op2];
    	dp = op3;
}
// =========
void n3(int op1, int op2, int op3)
{
	window->status = "Square root UNIMPLEMENTED! (-3)";
}
// =========
void n4(int op1, int op2, int op3)
{
	window->status = string("Checking if ") + to_string(data[op1]) +
		         string(" does not equal ") + to_string(data[op2]) +
			 string("... ");

	window->refreshData(ip-1, dp, next_card-1);

    	if (data[op1] != data[op2])
	{
		window->status = window->status + string(" True! Jumping to L") + to_string(op3);
        	ip = op3;
    	}
    	else
    	{
	    	window->status = window->status + string("False, moving on to next instruction.");
    	}
}
// =========
void n5(int op1, int op2, int op3)
{
    	window->status = string("Checking if ") + to_string(data[op1]) +
		         string(" is less than ") + to_string(data[op2]) + string("... ");

	window->refreshData(ip-1, dp, next_card-1);
    	if (data[op1] < data[op2])
	{
		window->status = window->status + string("True! Jumping to L") + to_string(op3);
        	ip = op3;
    	}
    	else
    	{
	    	window->status = window->status + string("False, moving on to next instruction.");
    	}
}
// =========
void n6(int op1, int op2, int op3)
{
	window->status = string("Moving value ") + to_string(data[op1]) +
		         string(" to location ") + to_string(op2+data[op3]);

    	data[op2+data[op3]] = data[op1];
	dp = op2 + data[op3];
}
// =========
void n7(int op1, int op2, int op3)
{
	window->status = string("Unused operation (-7)");
}
// =========
void n8(int op1, int op2, int op3)
{
	window->status = string("Sending contents of ") + to_string(op1) +
		         string(" to output");

     	window->output(to_string(data[op1]));
}
// =========
void n9(int op1, int op2, int op3)
{
	window->status = string("Unused operation (-9)");
}
// ================ loader  ==========================================
void loader(int datamem[],INSTRUCTIONS instrmem[],int cardreader[], string file_name)
{
	char FLAG[] = "+9999999999";
	int count = 0;
	int key_value;
	char buffer[256];

	//1) DECLARE A FILE VARIABLE (FV)
	fstream infile;
	//2) ASSIGN PHYSICAL FILE TO FV
	//3) OPEN FILE IN THE CORRECT FORMAT
	infile.open(file_name,ios::in);
	if(!infile)
	{
		cout << "ERROR - File not opened!" << endl;
		exit(1);
	}
	//4) USE FV IN I/O
	//===== READ DATA CARDS

	infile.getline(buffer,256,'\n');  // read first card

	while((!infile.fail())&& (strncmp(buffer,FLAG,11)))
	{
		int d = atoi(buffer);
		data[count] =d;
		count++;
		infile.getline(buffer,256,'\n'); // read next card
	}

	// ===== READ INSTRUCTION CARDS
	count = 0; //reset count
	infile.getline(buffer,256,'\n');  // read card after +9999999999
	while((!infile.fail()) && (strncmp(buffer,FLAG,11)))
	{
		parse_instruction(buffer, instrmem, count);
		count++;
		infile.getline(buffer,256,'\n');
	}

	//====== READ INPUT CARDS
	count = 0; //reset count
	if (infile.fail()) return;
	infile.getline(buffer,256,'\n');  // read card after +9999999999
	while(!infile.fail())
	{
	    int d = atoi(buffer);
		card[count] = d;
		count++;
		infile.getline(buffer,256,'\n');
	}

	//5) CLOSE THE FILE
	infile.close();
}
