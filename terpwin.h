/* class TerpWindow
 *
 * Creates ncurses window to step through "punch-card interpreter"
 * program. Helps visualize current instruction being executed, which
 * input card is being interpreted, and current and changing states of
 * the memory array. Verbosley explains what current instruction is
 * doing and shows output.
 */

#include <ncurses.h>
#include <string>

#include <instruct.h>

using namespace std;

class TerpWindow
{
	public:
		string status;

		TerpWindow(const int*, const int*, const INSTRUCTIONS*);
		~TerpWindow(void);
		void refreshData(const int, const int, const int);
		void refreshData(void);
		void initialize(void);
		void output(const string);
	private:
		const int *_data;
		const int *_cards;
		const INSTRUCTIONS *_program;
		int _dataRow;
		int _programRow;
		int _inputRow;
		int _outputx;
		int _outputy;
		int _maxHeight;
		int _maxWidth;
		int _selected;
		int _lastIP;
		int _lastDP;
		int _lastNC;

		void _columnSelect(void);
		void _shiftRow(int);
		void _shiftColumn(int);
		int _keyPress(int);

};

// TerpWindow Constructor
// Arguements: *dataArray - pointer to memory array
// 	       *cardArray - pointer to input card array
// 	       *programArray - pointer to array of program instructions.

TerpWindow::TerpWindow(const int *dataArray, const int *cardArray, const INSTRUCTIONS *programArray)
{
	// Initialize object variables.
	_dataRow = 0;
	_programRow= 0;
	_inputRow = 0;
	status = "Awaiting start...";
	_outputy = 17;
	_outputx = 10;
	_selected = 1;

	// Ncurses initialization
	initscr();
	getmaxyx(stdscr, _maxHeight, _maxWidth);
	cbreak();
	keypad(stdscr, TRUE);
	noecho();
	curs_set(0);
	start_color();
	init_pair(1, COLOR_GREEN, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);

	// Carefully check if good pointers were sent
	try
	{
		_data = dataArray;
		_cards = cardArray;
		_program = programArray;
	}
	catch(exception& e)
	{
		cout << "Error: " << e.what() << endl;
	}
}

// TerpWindow decontstructor
TerpWindow::~TerpWindow()
{
	endwin();
}

// Method to refresh data, program, and input columns
// Arguements: ip - current instruction index
// 	       dp - last modified data index
// 	       card - current input card being processed
void TerpWindow::refreshData(const int ip, const int dp, const int card)
{
	// remember last indexes
	_lastIP = ip;
	_lastDP = dp;
	_lastNC = card;

	// variables
	int value;
	int ch;
	INSTRUCTIONS c;
	int cycle = 1;
	int r;

	// If action is not currently visible, realign rows within proper range. 
	if(dp < _dataRow || dp > _dataRow + 9 )
	{
		_dataRow = dp - 5;
		if(_dataRow < 0) _dataRow = 0;
	}
	if(ip < _programRow || ip > _programRow + 9 )
	{
		_programRow = ip - 5;
		if(_programRow < 0) _programRow = 0;
	}
	if(card < _inputRow || card > _inputRow + 9)
	{
		_inputRow = card - 5;
		if(_inputRow < 0) _inputRow = 0;
	}

	// Allow for recycling refresh to shift rows/columns with out stepping forward
	while (cycle)
	{
		// Populate Data rows
		r = 0;
		for(int d = _dataRow; d < 10+_dataRow; d++)
		{
			value = _data[d];

			move(4+r,2);
			if (dp == d) attron(COLOR_PAIR(2)); // Colorize indexed data row
			printw("%d   ", d);
			move(4+r,9);
			printw("% d  ", value);
			attroff(COLOR_PAIR(2));             // Turn off color attribute
			r++;
		}

		// Populate Program rows
		r = 0;
		for(int i= _programRow; i < 10+_programRow; i++)
		{
			c = _program[i];
			if (c.sign)                              // Print only if structure populated
			{
				move(4+r,25);
				if (ip == i) attron(A_STANDOUT); // Highlight current instruction
				printw("%d   ", i);
				move(4+r,27);
				printw(" %3c%c  %03d  %03d  %03d", c.sign, c.oper, c.op1, c.op2, c.op3);
				attroff(A_STANDOUT);             // Turn off highlighting
			}
			else
			{
				move(4+r, 25);
				printw("%d                      ", i);  // Print nothing if struct is empty
			}
			r++;
		}

		// Populate Input rows
		r = 0;
		for(int c = _inputRow; c < 10+_inputRow; c++)
		{
			value = _cards[c];
			move(4+r,52);
			if (card == c) attron(COLOR_PAIR(1));   // Colorize if indexed input card
			printw("%d   ", c);
			move(4+r, 60);
			printw("   % d   ", value);

			attroff(COLOR_PAIR(1));                 // Turn off color
			r++;
		}

		// Clear status lines to prevent previous writes from lingering
		move(16, 0);
		clrtoeol();
		move(15,10);
		clrtoeol();
		printw("%s", status.c_str());

		// Move curser to next output location
		move(_outputy, _outputx);
		refresh();                // Refresh window
		ch = getch();             // Pause until key pressed
		cycle = _keyPress(ch);    // Processes key pressed to determine if cycle continues
	} //while

}

// Refresh the screen with last indexes
void TerpWindow::refreshData()
{
	refreshData(_lastIP,_lastDP,_lastNC);
}

// Draw layout to window
void TerpWindow::initialize()
{
   _columnSelect();

   move(2,2);
   printw("#     Value");
   move(2,25);
   printw("L     I   O1   O2   O3");
   move(2,52);
   printw("#     Value");
   move(15, 2);
   printw("Status: ");
   move(17, 2);
   printw("Output: ");
   move(20, 2);
   printw("Space: Step    Arrows/wsad/hjkl: Move rows/columns    Ctrl-C: Quit");
}

// Handle drawing column titles and selection highlighting
void TerpWindow::_columnSelect()
{
   char const *lables[] = {"Data:", "Program:", "Input:"};
   int count = sizeof(lables) / sizeof(char *);
   int x = 5;

   attron(A_BOLD);                        // Bold all
   for(int i = 0; i < count; i++)
   {
	if(_selected == i)                // Highlight selected column
	{
		attron(A_STANDOUT);
		move(1, x);
		printw("%s", lables[i]);
		attroff(A_STANDOUT);
	}
	else
	{
		move(1,x);
		printw("%s", lables[i]);
	}
	x += 25;

   }
   attroff(A_BOLD);                       // Turn off Bold
}

// Process keypress
// Arguements: ch - integer returned by ncurses getch()
// Note: ch is usually the ASCII decimal value. Otherwise,
// see curses.h for special characters
int TerpWindow::_keyPress(int ch)
{
	switch (ch)
	{
		case 0402 : // Down arrow
		case 115  : // s
		case 106  : // j
				_shiftRow(1); return 1;
		case 0403 : // Up arrow
		case 119  : // w
		case 107  : // k
				_shiftRow(-1); return 1;
		case 0404 : // Left arrow
		case 97   : // a
		case 104  : // h
				_shiftColumn(-1); return 1;
		case 0405 : // Right arrow
		case 100  : // d
		case 108  : // l
				_shiftColumn(1); return 1;
		case 32   : return 0; // Space
		default   : return 1; //"Ignore" other keys
	}
}

// Handles selection and wrapping of coloumn titles
// Arguements: shift - amount to move column selection (eg: -1 left, 1 right)
void TerpWindow::_shiftColumn(int shift)
{
	if (_selected + shift < 0)
		_selected = 2;
	else if (_selected + shift > 2)
		_selected = 0;
	else
		_selected += shift;

	_columnSelect();                // redraw columns
}

// Handles scrolling up and down rows of selected column
// Arguements: shift - amount to move rows up/down.
void TerpWindow::_shiftRow(int shift)
{
	int *sp = NULL;

	// Assign pointer based on which column is selected
	switch (_selected)
	{
		case 0 : sp = &_dataRow; break;
		case 1 : sp = &_programRow; break;
		case 2 : sp = &_inputRow; break;
	}


	// Shift rows and ensure still within bounds
	*sp = *sp  + shift;
	if (*sp < 0)
		*sp = 0;
	else if (*sp > 1000)
		*sp = 1000;
}

// Handles processing and curser managment for program outputs
// Arguements: outputString - output to be displayed
void TerpWindow::output(const string outputString)
{
	int length;

	length = outputString.length() + 2;

	getch();                  // Wait for input from keyboard

	// Handle wrapping of outputs
	try{
		if (_outputx + length >= _maxWidth)
		{
			if (_outputy + 1 >= _maxHeight)
				throw "Error: Output exceeded maximum range";

			_outputy += 1;
			_outputx = 10;
		}
	}
	catch (char *e) {
		endwin();
		cout << e;
	}

	// print output to screen and shift curser
	move(_outputy, _outputx);
	printw("%s, ", outputString.c_str());
	_outputx += length;
}

