# terpc

Ncurses implementation of a "punch-card" interpreter. (DSU CSC-461 Dr. Krebsbach) 

# Preamble

The original file "origin_sk2018.cpp" was provided and (I believe) written by Dr. Steven Krebsbach of Dakota State University. It has been provided for historical purporses and is not needed for the build.

My ultimate goal is to provide an Ncurses overlay with minimal changes to the original file.

# Usage

Build `g++ terpc.cpp -I . -lncurses`
Run `./terpc [inputfile.dat]`.

# Pull-requests

At DSU? Send 'em! Otherwise, send 'em!! (but why???)

