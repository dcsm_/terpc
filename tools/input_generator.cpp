#include <stdio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main(int argc, char **argv)
{
	int r_num;
	int count;

	// Check command line arguements
	if (argc < 2)
	{
		count = 10; // Default to 10 if not provided
	}
	else 
	{
		count = atoi(argv[1]);
	}

	// Initialize random seed
	srand(time(NULL));

	// First input card with number of following cards
	printf("+0000000%03d\n", count);
	
	for (int i = 0; i < count; i++)
	{

		// Generate number 0-999
		r_num = rand() % 1000;

		// Output random number in "Input card" format
		printf("+0000000%03d\n", r_num);
	}

	return 0;
}
